#!/usr/bin/env bash

(
  docker inspect mysql-var > /dev/null 2>&1
  RETVAL=$?
  if [ $RETVAL -ne 0 ]
      then
          docker create \
          --volume /var/lib/mysql \
          --name mysql-var \
          busybox
  fi
) || echo ""


docker rm -fv mysql || echo ""

docker run \
--env MYSQL_ALLOW_EMPTY_PASSWORD=TRUE \
--env MYSQL_ROOT_PASSWORD= \
--detach \
-p 3306:3306 \
--volumes-from mysql-var \
--name mysql \
mysql:5.5


docker run \
--detach \
--volume $(pwd)/server:/server \
--link mysql:mysql \
-p 3000:3000 \
--name loopback \
node:4.2.4-wheezy \
tail -f /var/log/dmesg