#!/usr/bin/env bash

docker run \
-it \
--volume $(pwd)/server:/server \
--link mysql:mysql \
node:4.2.4-wheezy \
/bin/bash