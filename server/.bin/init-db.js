var path = require('path');
var app = require(path.resolve('./server/server.js'));

var dataSource = app.dataSources.mysql;

dataSource.automigrate(function(err) {
  if (err) console.log(err);

  var Person = app.models.Person;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  Person.create([
    {firstName: 'Admin', lastName: 'User', username: 'adminuser', email: 'user@cqu.edu.au', password: 'changeme'}
  ], function(err, users) {
    if (err) return console.log(err);

    //create the admin role
    Role.create({
      name: 'admin'
    }, function(err, role) {
      if (err) return console.log(err);

      //make bob an admin
      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: users[0].id
      }, function(err, principal) {
        if (err) console.log(err);

        console.log('Done!')
      });
    });
  });

});

