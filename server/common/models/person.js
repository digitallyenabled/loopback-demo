module.exports = function(Person) {

  //Person.disableRemoteMethod("create", true);
  Person.disableRemoteMethod("upsert", true);
  Person.disableRemoteMethod("updateAll", true);
  //Person.disableRemoteMethod("updateAttributes", false);
  Person.disableRemoteMethod("find", true);
  Person.disableRemoteMethod("findById", true);
  Person.disableRemoteMethod("findOne", true);
  Person.disableRemoteMethod("deleteById", true);
  Person.disableRemoteMethod("confirm", true);
  Person.disableRemoteMethod("count", true);
  Person.disableRemoteMethod("exists", true);
  //Person.disableRemoteMethod("resetPassword", true);

  Person.disableRemoteMethod('getChangeStream', true);
  Person.disableRemoteMethod('createChangeStream', true);

  Person.disableRemoteMethod('__count__accessTokens', false);
  Person.disableRemoteMethod('__create__accessTokens', false);
  Person.disableRemoteMethod('__delete__accessTokens', false);
  Person.disableRemoteMethod('__destroyById__accessTokens', false);
  Person.disableRemoteMethod('__findById__accessTokens', false);
  Person.disableRemoteMethod('__get__accessTokens', false);
  Person.disableRemoteMethod('__updateById__accessTokens', false);

  Person.disableRemoteMethod('__count__entries', false);
  //Person.disableRemoteMethod('__create__entries', false);
  Person.disableRemoteMethod('__delete__entries', false);
  Person.disableRemoteMethod('__destroyById__entries', false);
  Person.disableRemoteMethod('__findById__entries', false);
  //Person.disableRemoteMethod('__get__entries', false);
  //Person.disableRemoteMethod('__updateById__entries', false);


  Person.sayHello = function (message, body, cb) {

    if (!!body) {
      return cb(null, body);
    } else {
      return cb(null, 'message');
    }

  };

  Person.remoteMethod(
    'sayHello',
    {
      accepts: [
        {arg: 'body', type: 'object', required: false, http: {source: 'body'}},
        {arg: 'message', type: 'string', required: true, http: {source: 'query' }, description: 'What is the message you want to send.'}
      ],
      http: { path: '/say-hello', verb: 'post'},
      returns: {arg: 'message', type: 'string'}
    }
  );

  // This is an example
  Person.updatePassword = function (message, userId, cb) {

    var app = Person.app;

    Person.findById(userId, function (err, thePerson) {

      thePerson.password = newPassword;
      thePerson.save();


    });


  };


  Person.on('resetPasswordRequest', function (info) {
    var app = Person.app;
    var Email = app.models.Email;

    var newPassword = "";
    var possibleCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 16; i++) {
      newPassword += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
    }

    info.user.password = newPassword;
    info.user.save();

    Email.send({
        to: info.email,
        from: 'engagecommunicatortesting@gmail.com', // TODO: UPDATE THIS TO THE CORRECT EMAIL ADDRESS
        subject: "Password reset",
        text: "Hello!\n\n" +
        "For temporary access to your account please use the following password\n\n" +
        newPassword + "\n\n" +
        "Once you have logged in, please update your password to something more memorable.\n",
        html: 'HTML'
      },
      function (err, value) {
        console.log(err, value);
      }
    );
  });
};
