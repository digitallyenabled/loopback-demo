module.exports = function (Entry) {

  Entry.disableRemoteMethod("create", true);
  Entry.disableRemoteMethod("upsert", true);
  Entry.disableRemoteMethod("updateAll", true);
  Entry.disableRemoteMethod("updateAttributes", false);
  Entry.disableRemoteMethod("find", true);
  Entry.disableRemoteMethod("findById", true);
  Entry.disableRemoteMethod("findOne", true);
  Entry.disableRemoteMethod("deleteById", true);
  Entry.disableRemoteMethod("count", true);
  Entry.disableRemoteMethod("exists", true);

  Entry.disableRemoteMethod("__get__person", false);

  Entry.disableRemoteMethod("getChangeStream", true);
  Entry.disableRemoteMethod("createChangeStream", true);

  Entry.getCsv = function (dateFrom, dateTo, cb) {

    try {
      var dateFromInstance = new Date(dateFrom).getTime();
      var dateToInstance = new Date(dateTo).getTime();
    } catch (exception) {
      var error = new Error('Invalid dates given');
      error.statusCode = 400;
      error.code = 'INVALID_DATES_GIVEN';
      return cb(err);
    }

    var filter = {
      where: {
        and: [
          {
            entryDate: {
              gt: dateFromInstance
            }
          },
          {
            entryDate: {
              lt: dateToInstance
            }
          }
        ]
      }
    };
    Entry.find(filter, function (err, entries) {
      if (err) {
        cb(err);
      } else if (entries) {
        cb(null, entries);
      } else {
        var error = new Error('no entries found');
        error.statusCode = 404;
        error.code = 'NO_ENTRIES_FOUND';
        cb(error);
      }
    });
  }
  ;

  Entry.remoteMethod('getCsv', {
    description: 'Downloads entries as a csv file',
    accepts: [
      {arg: 'dateFrom', type: 'string', required: true},
      {arg: 'dateTo', type: 'string', required: true}
    ],
    returns: {
      arg: 'data', type: 'blob',
      description: 'The response is a downloaded CSV file.'
    },
    http: {path: '/get-csv', verb: 'get'}
  });

  Entry.afterRemote('getCsv', function (ctx, affectedModelInstance, next) {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + " " + +(currentdate.getMonth() + 1) + " " + +currentdate.getFullYear() + " " + +currentdate.getHours() + ":"
      + currentdate.getMinutes() + ":"
      + currentdate.getSeconds();
    ctx.res.set('Expires', 'Tue, 03 Jul 2001 06:00:00 GMT');
    ctx.res.set('Cache-Control', 'max-age=0, no-cache, must-revalidate, proxy-revalidate');
    ctx.res.set('Last-Modified', datetime + 'GMT');
// force download
    ctx.res.set('Content-Type', 'application/force-download');
    ctx.res.set('Content-Type', 'application/octet-stream');
    ctx.res.set('Content-Type', 'application/download');
// disposition / encoding on response body
    ctx.res.set('Content-Disposition', 'attachment;filename=session-results.csv');
    ctx.res.set('Content-Transfer-Encoding', 'binary');

    var results = ctx.result.data;
    var csvData = "No Results";
    if (results.length) {
      // This formats an array as a CSV row. i.e. "value1","value2","value3"
      var convertRowToCSVFormat = function (array) {
        var resultText = array.join(';;;');
        resultText = resultText.replace(/"/g, "'");
        resultText = resultText.replace(/;;;/g, '","');

        return '"' + resultText + "\"\n";
      };

      var columns = Object.keys(results[0].toObject()), // Define the order of the properties to include on rows
        rowArray,
        result,
        propertyName, propertyValue, parsedPropertyValue;

      // Start the CSV output as a list of all the column names.
      // i.e. ['Creation', 'Modification']
      csvData = convertRowToCSVFormat(columns); // This is the headings row, you can add 'pretty' headings here.
      // Iterate over each of the entries in the data.

      // This section is done quickly to convert all values into an array. You could take an explicit approach and
      // define each property that needs to be in the returned CSV such as the example below

      // rowArray['creation'] = result['creation']
      // rowArray['modification'] = result['modification']
      // rowArray[...] = result[...]

      for (var resultIndex = 0; resultIndex < results.length; resultIndex++) {
        rowArray = [];
        result = results[resultIndex].toObject();
        // For each of the column names
        for (var columnIndex = 0; columnIndex < columns.length; columnIndex++) {
          propertyName = columns[columnIndex];
          propertyValue = result[propertyName];
          switch (true) {
            // Add any other logic for property values here
            default:
              parsedPropertyValue = propertyValue;
          }

          // Push this current entry rows property into the row array
          rowArray.push(parsedPropertyValue);
        }
        // Now convert this rowArray into CSV format.
        csvData += convertRowToCSVFormat(rowArray);
      }
    }

    // Return the csv data
    ctx.res.send(csvData);

  }, function (err, response) {
    if (err) console.error(err);
    next();
  });
}
;
